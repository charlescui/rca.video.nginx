class Img
	attr_accessor :name, :ts, :channel
	def initialize(opts={})
		self.ts = opts[:ts]
		self.channel = opts[:channel]
		self.name = "#{opts[:ts]}.jpg"
	end
	
	def style_urls
		h = {}
		[:large, :small].each { |style|  
			h[style] = "/#{self.channel}/#{self.name}"
		}
		h
	end
end