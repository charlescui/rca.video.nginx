#include "mruby.h"
#include "mruby/class.h"
#include "mruby/value.h"
#include "mruby/hash.h"
#include "mruby/data.h"
#include "mruby/variable.h"
#include "mruby/string.h"
#include "mruby/array.h"
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static struct RClass *Snap;

#define DONE mrb_gc_arena_restore(mrb, 0)
#define SNAP_KEY            "$snap_key"

void
none_free(mrb_state *mrb, void *p){}

static const struct mrb_data_type snap_type = { 
    SNAP_KEY, none_free
};

typedef struct {
    size_t      len;
    u_char     *data;
} ngx_str_t;

typedef intptr_t        ngx_int_t;

extern void*       ngx_snapshot_get_shm(ngx_str_t mmap_file);
extern ngx_int_t   ngx_snapshot_retrieve_name(void *header, ngx_str_t channel, time_t target_ts, uint32_t range, time_t* buffer);
extern ngx_int_t   ngx_snapshot_raw(void *header, ngx_str_t channel, time_t target_ts);
extern ngx_int_t   ngx_snapshot_large(void *header, ngx_str_t channel, time_t target_ts);
extern ngx_int_t   ngx_snapshot_small(void *header, ngx_str_t channel, time_t target_ts);

static mrb_value 
snap_write_image_by_style(mrb_state *mrb, mrb_value self){
    mrb_value style, ts, channel;
    ngx_str_t ngx_channel;
    time_t c_ts;
    ngx_int_t code = 0, act = -1;
    void **shm_addr;
    char buffer[256]={0}, buffer2[256]={0}, c_channel[256]={0};

    mrb_get_args(mrb, "ooo", &channel, &style, &ts);
    Data_Get_Struct(mrb, self, &snap_type, shm_addr);

    ngx_channel.data = c_channel;
    ngx_channel.len = RSTRING_LEN(channel);
    strncpy(ngx_channel.data,RSTRING_PTR(channel), RSTRING_LEN(channel));
    c_ts = mrb_fixnum(ts);

    if (strncmp(RSTRING_PTR(style), "raw", RSTRING_LEN(style)) == 0)
    {
        code = ngx_snapshot_raw(*shm_addr, ngx_channel, c_ts);
        act = 0;
    }else if (strncmp(RSTRING_PTR(style), "large", RSTRING_LEN(style)) == 0)
    {
        code = ngx_snapshot_large(*shm_addr, ngx_channel, c_ts);
        act = 1;
    }else if (strncmp(RSTRING_PTR(style), "small", RSTRING_LEN(style)) == 0)
    {
        code = ngx_snapshot_small(*shm_addr, ngx_channel, c_ts);
        act = 2;
    }else{
        return mrb_nil_value();
    }

    if (code == 0)
    {
        ts = mrb_funcall(mrb, ts, "to_s", 0, NULL);
        switch(act){
            case 0:
                strncat(buffer, RSTRING_PTR(channel), RSTRING_LEN(channel));
                sprintf(buffer2, "/%s/%ld.jpg", buffer, c_ts);
                break;
            case 1:
                strncat(buffer, RSTRING_PTR(channel), RSTRING_LEN(channel));
                buffer[strlen(buffer)] = '/';
                strncat(buffer, RSTRING_PTR(style), RSTRING_LEN(style));
                sprintf(buffer2, "/%s/%ld.jpg", buffer, c_ts);
                break;
            case 2:
                strncat(buffer, RSTRING_PTR(channel), RSTRING_LEN(channel));
                buffer[strlen(buffer)] = '/';
                strncat(buffer, RSTRING_PTR(style), RSTRING_LEN(style));
                sprintf(buffer2, "/%s/%ld.jpg", buffer, c_ts);
                break;
            default:
            return mrb_false_value();
        }

        return mrb_str_new_cstr(mrb, buffer2);
    }else{
        return mrb_false_value();
    }
}

static mrb_value 
snap_fetch_by_ngx_snap_module(mrb_state *mrb, mrb_value self){
	mrb_value h, channel, count, ts, snaps;
    void **shm_addr;
    int i = 0, c_count, c_ts;
    ngx_str_t ngx_channel;
    // time_t buffer[900];
    time_t* buffer;

  	mrb_get_args(mrb, "H", &h);

  	channel = mrb_hash_get(mrb, h, mrb_str_new_cstr(mrb, "channel"));
  	count = mrb_hash_get(mrb, h, mrb_str_new_cstr(mrb, "count"));
  	ts = mrb_hash_get(mrb, h, mrb_str_new_cstr(mrb, "ts"));

  	if (mrb_type(channel) != MRB_TT_STRING) {
        channel = mrb_funcall(mrb, channel, "to_s", 0, NULL);
    } 

  	if (mrb_type(count) != MRB_TT_FIXNUM) {
        count = mrb_funcall(mrb, count, "to_i", 0, NULL);
    } 

  	if (mrb_type(ts) != MRB_TT_FIXNUM) {
        ts = mrb_funcall(mrb, ts, "to_i", 0, NULL);
    } 

    // get value from mruby value
    c_count = mrb_fixnum(count);
    c_ts = mrb_fixnum(ts);
    ngx_channel.len = RSTRING_LEN(channel);
    ngx_channel.data = RSTRING_PTR(channel);

    Data_Get_Struct(mrb, self, &snap_type, shm_addr);

    // init memory
    buffer = malloc(c_count*sizeof(time_t));
    if (!buffer)
    {
        return mrb_nil_value();
    }
    memset(buffer, 0, c_count*sizeof(time_t));
    snaps = mrb_ary_new_capa(mrb, c_count);

    // call nginx snap module fetch images func
    ngx_int_t r = ngx_snapshot_retrieve_name(*shm_addr, ngx_channel, c_ts, c_count, buffer);
    time_t tmp;
    for (; i < r; ++i)
    {
        tmp = buffer[i];
        mrb_ary_push(mrb, snaps, mrb_fixnum_value(tmp));
    }
    free(buffer);
    return snaps;
}

static mrb_value 
snap_init_shm_by_ngx_snap_module(mrb_state *mrb, mrb_value self){
    mrb_value file;
    mrb_get_args(mrb, "o", &file);
    ngx_str_t mmap_file;
    void **shm_addr;

    if (mrb_type(file) != MRB_TT_STRING) {
        file = mrb_funcall(mrb, file, "to_s", 0, NULL);
    } 

    mmap_file.len = RSTRING_LEN(file);
    mmap_file.data = RSTRING_PTR(file);

    shm_addr = (void *)mrb_malloc(mrb, sizeof(void *));
    // call nginx snap module init share memory func
    *shm_addr = ngx_snapshot_get_shm(mmap_file);

    if (!shm_addr) mrb_raise(mrb, E_RUNTIME_ERROR, "memory allocation error");

    return mrb_obj_value((void *)Data_Wrap_Struct(mrb, mrb_class_ptr(self), &snap_type, shm_addr));
}

void 
mrb_snap_gem_init(mrb_state *mrb) {
	Snap = mrb_define_class(mrb, "Snap", mrb->object_class);
	mrb_define_singleton_method(mrb, (struct RObject*)Snap, "new", snap_init_shm_by_ngx_snap_module, MRB_ARGS_REQ(1));
	mrb_define_method(mrb, (struct RObject*)Snap, "fetch", snap_fetch_by_ngx_snap_module, MRB_ARGS_REQ(1));
    mrb_define_method(mrb, (struct RObject*)Snap, "write", snap_write_image_by_style, MRB_ARGS_REQ(3));

	DONE;
}

void
mrb_snap_gem_final(mrb_state* mrb)
{
}
