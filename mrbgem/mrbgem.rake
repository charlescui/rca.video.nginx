MRuby::Gem::Specification.new('snap') do |spec|
  spec.license = 'MIT'
  spec.authors = 'Charles Cui'

  src_root = File.join(File.dirname(__FILE__), "src")
  snapshot_root = File.join(File.dirname(__FILE__), "../snapshot")
  ['ngx_snapshot_shm.*', 'ngx_snapshot_video.*'].each do |file|
  	`cp #{snapshot_root}/#{file} #{File.dirname(src_root)}`
  end
  ngx_root = "/usr/local/src/rca.video.nginx.docker/build/nginx_src"
  spec.cc.include_paths << "#{ngx_root}/objs"
  spec.cc.include_paths << "#{ngx_root}/src/core"
  spec.cc.include_paths << "#{ngx_root}/src/event"
  spec.cc.include_paths << "#{ngx_root}/src/event/modules"
  spec.cc.include_paths << "#{ngx_root}/src/os/unix"
  spec.cc.include_paths << "#{ngx_root}/src/proc"
  spec.cc.include_paths << "/usr/local/include/ImageMagick-6"
  spec.cc.include_paths << "#{ngx_root}/objs"
  spec.cc.include_paths << "#{ngx_root}/src/http"
  spec.cc.include_paths << "#{ngx_root}/src/http/modules"
  spec.cc.include_paths << "#{ngx_root}/src/mail"

  spec.linker.libraries << "avformat"
  spec.linker.libraries << "avcodec"
  spec.linker.libraries << "dl"
  spec.linker.libraries << "z"
  spec.linker.libraries << "rt"
  spec.linker.libraries << "swscale"
  spec.linker.libraries << "avutil"
  spec.linker.libraries << "m"
  spec.linker.libraries << "MagickWand-6.Q16"
  spec.linker.libraries << "MagickCore-6.Q16"
  spec.linker.libraries << "swresample"
  spec.linker.libraries << "gvplugin_core"
  spec.linker.libraries << "gvplugin_dot_layout"

  spec.linker.library_paths << "/usr/local/lib"
  spec.linker.library_paths << "/usr/lib/graphviz"
  spec.cc.flags << "-W"
end
