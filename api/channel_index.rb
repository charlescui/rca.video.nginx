# /channels/tianjinweishi?count=1
$snap = Snap.new("/tmp/snapshot_mmap_file") unless $snap

r = Nginx::Request.new()
uri = r.var.uri
channel = uri.split('/')[2]
count = ((r.var.arg_count and r.var.arg_count != '0') ? r.var.arg_count : 10).to_i
ts = ((r.var.arg_ts and r.var.arg_ts != '0') ? r.var.arg_ts : Time.now).to_i

if channel
	ts_arr = $snap.fetch("count" => count, "channel" => channel, "ts" => ts)
	imgs = ts_arr.map { |e| 
		%Q-{"name":"#{e}.jpg","ts":#{e},"urls":{"large":"/#{channel}/large/#{e}.jpg","small":"/#{channel}/small/#{e}.jpg"}}-
	}
	json_prefix = %Q-{"status":0,"data":{"channel":"#{channel}","files":[-
	json_sufix = "]}}"
	json = "#{json_prefix}#{imgs.join(',')}#{json_sufix}"
else
	hash = {
		:status => -1,
		:msg => 'error for channel'
	}
	json = JSON.stringify(hash)
end


Nginx.rputs json
Nginx.return Nginx::NGX_HTTP_OK