# /xizangweishi/large/1373873571.jpg
$snap = Snap.new("/tmp/snapshot_mmap_file") unless $snap

r = Nginx::Request.new()
uri = r.var.uri
paths = uri.split('/')

case paths.size
when 3
	channel = paths[1]
	style = "raw"
	if paths[2]
		ts, ext = paths[2].split('.')
	end
when 4
	channel = paths[1]
	style = paths[2]
	if paths[3]
		ts, ext = paths[3].split('.')
	end
end


if (file = $snap.write(channel, style, ts.to_i))
	Nginx.redirect(file)
else
	Nginx.rputs "No image file found."
	Nginx.return Nginx::NGX_HTTP_NOT_FOUND
end