#ifndef     H_NGX_SANPSHOT_H__
#define     H_NGX_SNAPSHOT_H__
#include    "ngx_snapshot_shm.h"
#include    <wand/MagickWand.h>
#include    <libavcodec/avcodec.h>
#include    <libavformat/avformat.h>

#define     RAW_WIDTH       768
#define     RAW_HEIGHT      576
#define     LARGE_WIDTH     612
#define     LARGE_HEIGHT    482
#define     SMALL_WIDTH     143
#define     SMALL_HEIGHT    113
#define     CROP_OFFSET_X   25
#define     CROP_OFFSET_Y   5

#define ThrowWandException(wand) \
{ \
  char \
    *description; \
 \
  ExceptionType \
    severity; \
 \
  description=MagickGetException(wand,&severity); \
  (void) fprintf(stderr,"%s %s %lu %s\n",GetMagickModule(),description); \
  description=(char *) MagickRelinquishMemory(description); \
  exit(-1); \
}

/*size style*/
typedef enum STYLE_E
{
    SMALL = 0x01,
    LARGE = 0x02,
    RAW = 0x03
} STYLE;

ngx_int_t   ngx_snapshot_decode_stream(snapshot_circular_buffer_t* cbuffer); 
ngx_int_t   ngx_snapshot_write_jpeg(snapshot_circular_buffer_t *cbuffer, AVCodecContext *pAVCodecCtx, AVFrame *pFrame);
ngx_int_t   ngx_snapshot_crop_image(snapshot_slot_t* slot, const char* outfile, size_t width, size_t height, ssize_t x, ssize_t y);
ngx_int_t   ngx_snapshot_resize_image(snapshot_slot_t *slot, const char* outfile, size_t width, size_t height);
ngx_int_t   ngx_snapshot_orig2style(snapshot_slot_t *slot, const char* outfile, STYLE s);
ngx_int_t   ngx_snapshot_raw(snapshot_shm_header_t *header, ngx_str_t channel, time_t target_ts);
ngx_int_t   ngx_snapshot_large(snapshot_shm_header_t *header, ngx_str_t channel, time_t target_ts);
ngx_int_t   ngx_snapshot_small(snapshot_shm_header_t *header, ngx_str_t channel, time_t target_ts);
#endif  /*H_NGX_SNAPSHOT_H__*/
