#include "ngx_snapshot_shm.h"
#include "ngx_snapshot_video.h"

#include <ngx_event.h>
#include <ngx_core.h>
#include <ngx_config.h>

#include <sys/mman.h>



struct  ngx_proc_snapshot_ctx_s;
struct  ngx_proc_snapshot_conf_s;

typedef struct ngx_proc_snapshot_ctx_s ngx_proc_snapshot_ctx_t; 
typedef struct ngx_proc_snapshot_conf_s ngx_proc_snapshot_conf_t; 

typedef struct {
    snapshot_circular_buffer_t  *cbuffer;

    /*reserved, not used now*/
    ngx_shm_zone_t              *shm_zone;
    ngx_slab_pool_t             *shpool;
} ngx_thread_snapshot_ctx_t;

struct ngx_proc_snapshot_ctx_s{
    snapshot_shm_header_t       *shm_header;
    ngx_event_t                 timeout_ev;
    ngx_proc_snapshot_conf_t    *ss_conf;
    ngx_int_t                   terminate;
    ngx_uint_t                  shm_size;

    /*reserved, not used now*/
    ngx_shm_zone_t              *shm_zone;
    ngx_slab_pool_t             *shpool;

};

struct ngx_proc_snapshot_conf_s{
    ngx_str_t                   mmap_file;
    ngx_array_t                 udp_address;
    ngx_uint_t                  nslots;
    ngx_str_t                   snap_root;
    ngx_uint_t                  missed_warning;
    ngx_proc_snapshot_ctx_t     *ss_ctx;

};

static void *ngx_proc_snapshot_create_conf(ngx_conf_t *cf);
static char *ngx_proc_snapshot_merge_conf(ngx_conf_t*cf, void *parent, void* child);
static ngx_int_t ngx_proc_snapshot_prepare(ngx_cycle_t *cycle);
static ngx_int_t ngx_proc_snapshot_process_init(ngx_cycle_t *cycle);
static ngx_int_t ngx_proc_snapshot_loop(ngx_cycle_t *cycle);
static void ngx_proc_snapshot_exit_process(ngx_cycle_t *cycle);
static void ngx_proc_snapshot_update(ngx_event_t *ev);
static void ngx_proc_snapshot_clean(const char *path, time_t curr_time, ngx_uint_t lifetime);
static void ngx_proc_snapshot_dofork(snapshot_shm_header_t *header, uint32_t index);

static ngx_int_t ngx_proc_snapshot_init_shm(ngx_cycle_t *cycle);
static ngx_int_t ngx_proc_snapshot_child_routine(void* args);

static char* ngx_conf_set_udp_address(ngx_conf_t *cf, ngx_command_t *cmd, void *conf);
static char* ngx_conf_set_shm(ngx_conf_t *cf, ngx_command_t *cmd, void *conf);

/*signal related functions*/
ngx_int_t    ngx_proc_snapshot_init_signals(ngx_log_t* log);
void    ngx_proc_snapshot_signal_handler(int signo);
void    ngx_proc_snapshot_stop_child(ngx_proc_snapshot_ctx_t *ss_ctx);
void    ngx_proc_snapshot_reap_child(ngx_proc_snapshot_ctx_t *ss_ctx);

/*periodic check*/
void    ngx_proc_snapshot_check(ngx_proc_snapshot_ctx_t *ss_ctx);


static ngx_command_t ngx_proc_snapshot_commands[] = {
    
    { ngx_string("mmap_file"),
      NGX_PROC_CONF|NGX_CONF_TAKE1,
      ngx_conf_set_shm,
      NGX_PROC_CONF_OFFSET,
      0,
      NULL },

    { ngx_string("udp_address_file"),
      NGX_PROC_CONF|NGX_CONF_TAKE1,
      ngx_conf_set_udp_address,
      NGX_PROC_CONF_OFFSET,
      0,
      NULL },
    
    { ngx_string("snapshot_nslots"),
      NGX_PROC_CONF|NGX_CONF_TAKE1,
      ngx_conf_set_num_slot,
      NGX_PROC_CONF_OFFSET,
      offsetof(ngx_proc_snapshot_conf_t, nslots),
      NULL },

    { ngx_string("snap_root"),
      NGX_PROC_CONF|NGX_CONF_TAKE1,
      ngx_conf_set_str_slot,
      NGX_PROC_CONF_OFFSET,
      offsetof(ngx_proc_snapshot_conf_t, snap_root),
      NULL },

    { ngx_string("missed_warning"),
      NGX_PROC_CONF|NGX_CONF_TAKE1,
      ngx_conf_set_num_slot,
      NGX_PROC_CONF_OFFSET,
      offsetof(ngx_proc_snapshot_conf_t, missed_warning),
      NULL },

      ngx_null_command
};
 
static ngx_proc_module_t ngx_proc_snapshot_module_ctx = {
    ngx_string("snapshot"),
    NULL,
    NULL,
    ngx_proc_snapshot_create_conf,
    ngx_proc_snapshot_merge_conf,
    ngx_proc_snapshot_prepare,
    ngx_proc_snapshot_process_init,
    ngx_proc_snapshot_loop,
    ngx_proc_snapshot_exit_process
};

ngx_module_t ngx_proc_snapshot_module = {
    NGX_MODULE_V1,
    &ngx_proc_snapshot_module_ctx,      /* module context */
    ngx_proc_snapshot_commands,         /* module directives */
    NGX_PROC_MODULE,                    /* module type */
    NULL,                               /* init master */
    NULL,                               /* init module */
    NULL,                               /* init process */
    NULL,                               /* init thread */
    NULL,                               /* exit thread */
    NULL,                               /* exit process */
    NULL,                               /* exit master */
    NGX_MODULE_V1_PADDING
};

static char*
ngx_conf_set_udp_address(ngx_conf_t *cf, ngx_command_t *cmd, void* conf)
{
    ngx_str_t                   *value;
    ngx_proc_snapshot_conf_t    *ss_conf;
    udp_entry_t                 *entry;
    FILE                        *fp;
    int32_t                     rc; 
    size_t                      len, delim_pos, end;
    char                        *line;

    line = NULL;
    ss_conf = conf;
    value = cf->args->elts;

    /*get file full name*/
    if (ngx_conf_full_name(cf->cycle, &value[1], 0) != NGX_OK) {
        return NGX_CONF_ERROR;
    }

    fp = fopen(value[1].data, "r");
    if ( fp == NULL ) {
        ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                                "open udp address file error");
        return NGX_CONF_ERROR;
    }

    if (ss_conf->udp_address.elts == NULL) {
        if (ngx_array_init(&ss_conf->udp_address, cf->pool, DEFAULT_ADDRESS_NUM, 
                                        sizeof(udp_entry_t) ) != NGX_OK)
        {
            return NGX_CONF_ERROR;
        }

    }

    while ((rc = getline(&line, &len, fp)) != -1) {
        /*lines start with # will been ignored*/
        if (*line == '#') {
            continue;
        }

        delim_pos = 0;
        while ( *(line + delim_pos) != ' ' && *(line + delim_pos) != '\t' && delim_pos < len) {
            delim_pos++;
        }

        if ( delim_pos == len ) {
            ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                                "udp address file format error");
            return NGX_CONF_ERROR;
        }

        entry = ngx_array_push(&ss_conf->udp_address);
        entry->address.len = delim_pos + 1;//add 1 for \0
        entry->address.data = ngx_pcalloc(cf->pool, entry->address.len);
        if (entry->address.data == NULL) {
            ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                                "snapshot parse udp address error");
            return NGX_CONF_ERROR;
        }
        ngx_copy(entry->address.data, line, entry->address.len);
        entry->address.data[entry->address.len - 1] = '\0';

        while ( *(line + delim_pos) == ' ' || *(line + delim_pos) == '\t' ) {

            delim_pos++;
        }

        end = delim_pos;
        while ( (line + end) != NULL && *(line + end) != '\n') {
            end++;
        }
        entry->name.len = end - delim_pos + 1; //add 1 for \0
        entry->name.data = ngx_pcalloc(cf->pool, entry->name.len);
        if (entry->name.data == NULL) {
            ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                                "snapshot parse udp address error");
            return NGX_CONF_ERROR;
        }
        ngx_copy(entry->name.data, line + delim_pos, entry->name.len);
        entry->name.data[entry->name.len - 1] = '\0';
        
    }
    fclose(fp);
    free(line);

    return NGX_CONF_OK;

}

static char*
ngx_conf_set_shm(ngx_conf_t *cf, ngx_command_t *cmd, void* conf)
{
    ngx_proc_snapshot_conf_t    *ss_conf;
    ngx_str_t                   *value;

    ss_conf = conf;
    value = cf->args->elts;
    ss_conf->mmap_file.len = value[1].len + 1;//add\0
    ss_conf->mmap_file.data = ngx_pcalloc(cf->pool, ss_conf->mmap_file.len);
    if (ss_conf->mmap_file.data == NULL) {
        ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                                "snapshot parse mmap file error");
        return  NGX_CONF_ERROR;
    }
    ngx_copy(ss_conf->mmap_file.data, value[1].data, ss_conf->mmap_file.len);
    ss_conf->mmap_file.data[ss_conf->mmap_file.len] = '\0';
    
    return NGX_CONF_OK;

}

static ngx_int_t
ngx_proc_snapshot_init_shm(ngx_cycle_t *cycle)
{
    ngx_proc_snapshot_conf_t    *ss_conf;
    ngx_proc_snapshot_ctx_t     *ss_ctx;
    ngx_uint_t                  nslots;
    snapshot_shm_header_t       *header;
    ss_array_t                  ss_address;
    ngx_uint_t                  shm_size;
    
    ss_conf = ngx_proc_get_conf(cycle->conf_ctx, ngx_proc_snapshot_module);
    nslots = ss_conf->nslots;

    /*convert ngx_array_t to ss_array_t*/
    ss_address.size = ss_conf->udp_address.size;
    ss_address.nelts = ss_conf->udp_address.nelts;
    ss_address.elts = ss_conf->udp_address.elts;
    
    ngx_snapshot_init_shm(&header, ss_conf->mmap_file, ss_address, ss_conf->snap_root, nslots, &shm_size);
    
    /*init ngx_proc_snapshot_ctx_t*/
    ss_ctx = ngx_pcalloc(cycle->pool, sizeof(ngx_proc_snapshot_ctx_t));
    if (ss_ctx == NULL) {
        ngx_log_error(NGX_LOG_EMERG, cycle->log, 0,
                                "snapshot snapshot context create error");
        return NGX_ERROR;
    }

    ss_conf->ss_ctx = ss_ctx;
    ss_ctx->shm_header = header;
    ss_ctx->shm_size = shm_size;
    ss_ctx->ss_conf = ss_conf;

    return NGX_OK;

}

/* for test purpose */
static void*
ngx_proc_snapshot_child_routine2(snapshot_circular_buffer_t *cbuffer)
{
    ngx_uint_t    *pflag = cbuffer->extract_flag;

    char    test[51200] = {"abcdefghijklmnopqrstuvwxyz"};

    while(1){
        if(*pflag == 1)
        {
            ngx_snapshot_insert_buffer(cbuffer, test, 51200);
            *pflag = 0;
        }
        usleep(100000);//100ms
    }

    return 0;
}

static void 
ngx_proc_snapshot_dofork(snapshot_shm_header_t *header, uint32_t index)
{
    ngx_log_error(NGX_LOG_EMERG, ngx_cycle->log, 0,
                    "snapshot prepare to start child. index:%d", index);
    if (fork() == 0) {

        snapshot_circular_buffer_t      *cbuffer;
        cbuffer = header->cbuffer + index;
        //fprintf(stderr, "pid:%d index:%d address:%u", getpid(), index, (unsigned int)cbuffer);
        *(cbuffer->pid) = getpid();
        ngx_proc_snapshot_child_routine(cbuffer);
        ngx_log_error(NGX_LOG_EMERG, ngx_cycle->log, 0,
                                    "snapshot child exit proc routine.%d", getpid());
        exit(0);
    }
}


static void*
ngx_proc_snapshot_create_conf(ngx_conf_t *cf)
{
    ngx_proc_snapshot_conf_t *conf;

    conf = ngx_pcalloc(cf->pool, sizeof(ngx_proc_snapshot_conf_t));

    if (conf == NULL) {
        ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                            "snapshot create proc conf error");
        return NULL;  
    }
    
    /*set to UNSET when use default parameter parser*/
    conf->nslots = NGX_CONF_UNSET;
    conf->missed_warning = NGX_CONF_UNSET;

    return conf;
}


static char*
ngx_proc_snapshot_merge_conf(ngx_conf_t *cf, void *parent, void *child)
{
    ngx_proc_snapshot_conf_t *prev = parent;
    ngx_proc_snapshot_conf_t *conf = child;

    ngx_conf_merge_str_value(conf->mmap_file, prev->mmap_file, "");

    return NGX_CONF_OK;
}

static ngx_int_t
ngx_proc_snapshot_prepare(ngx_cycle_t *cycle)
{
    ngx_proc_snapshot_conf_t *conf;

    conf = ngx_proc_get_conf(cycle->conf_ctx, ngx_proc_snapshot_module);
    if (conf->mmap_file.len == 0 || conf->udp_address.nelts == 0) {
        return NGX_DECLINED;
    }

    ngx_proc_snapshot_init_shm(cycle);
    
    return NGX_OK;
}


static ngx_int_t
ngx_proc_snapshot_process_init(ngx_cycle_t *cycle)
{
    ngx_proc_snapshot_conf_t    *conf;
    ngx_proc_snapshot_ctx_t     *ss_ctx;
    ngx_uint_t                  i, naddress, total_size;
    ngx_event_t                 *timeout_ev;
    ngx_int_t                   fd;
    void*                       shm_addr;


    conf = ngx_proc_get_conf(cycle->conf_ctx, ngx_proc_snapshot_module);
    ss_ctx = conf->ss_ctx;

    /*map shared file*/
    naddress = conf->udp_address.nelts;
    total_size = ss_ctx->shm_size;

    fd = open(conf->mmap_file.data, O_RDWR, 00777);
    if ( fd == -1) {
        ngx_log_error(NGX_LOG_EMERG, cycle->log, 0,
                                "snapshot open mmap file error");
        return NGX_ERROR;
    }

    shm_addr = mmap(NULL, total_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    if (shm_addr == (void*)-1)
    {
        ngx_log_error(NGX_LOG_EMERG, cycle->log, 0, 
                                "snapshot remmap error");
        return NGX_ERROR;
    }
    close(fd);

    ss_ctx->shm_header = shm_addr;
    ss_ctx->terminate = 0;

    /*init child process*/
    for ( i = 0; i < naddress; i++) {
        ngx_proc_snapshot_dofork(ss_ctx->shm_header, i);
    }
    
    /*register signals*/
    if (ngx_proc_snapshot_init_signals(cycle->log) == NGX_ERROR)
        return NGX_ERROR; 

    /*init timer*/
    timeout_ev = &(ss_ctx->timeout_ev);
    ngx_memzero(timeout_ev, sizeof(ngx_event_t));
    timeout_ev->log = cycle->log;
    timeout_ev->handler = ngx_proc_snapshot_update;
    timeout_ev->data = ss_ctx;
    ngx_add_timer(timeout_ev, SNAPSHOT_INTERVAL);
    
    return NGX_OK;
}

static ngx_int_t 
ngx_proc_snapshot_loop(ngx_cycle_t *cycle)
{
    ngx_log_error(NGX_LOG_INFO, cycle->log, 0, "in snapshot loop\n");

    return NGX_OK;
}

static void
ngx_proc_snapshot_exit_process(ngx_cycle_t *cycle)
{
    ngx_proc_snapshot_conf_t    *conf;
    ngx_proc_snapshot_ctx_t     *ss_ctx;

    conf = ngx_proc_get_conf(cycle->conf_ctx, ngx_proc_snapshot_module);
    ss_ctx = conf->ss_ctx;

    ngx_log_error(NGX_LOG_INFO, cycle->log, 0, 
                    "snapshot parent process is exiting. stop children.");
    ss_ctx->terminate = 1;
    ngx_proc_snapshot_stop_child(ss_ctx);
    
    while (1) {
        ngx_proc_snapshot_reap_child(ss_ctx);
    }
}

static void 
ngx_proc_snapshot_update(ngx_event_t *ev)
{
    ngx_uint_t                  i;
    struct timeval              tv_now;
    snapshot_shm_header_t       *header;
    ngx_str_t                   snap_root;
    char                        snap_dir[PATH_MAX_LEN] = {0};
    
    ngx_log_error(NGX_LOG_INFO, ev->log, 0, "timer expired.\n");
    ngx_proc_snapshot_ctx_t     *ss_ctx;
    ss_ctx = (ngx_proc_snapshot_ctx_t*)(ev->data);
    header = ss_ctx->shm_header;

    gettimeofday(&tv_now, NULL);	
    header->timestamp = tv_now.tv_sec;
    for (i = 0; i < header->udp_address.nelts; i++) {
        *(header->extract_flag + i) = 1;
    }
    
    ngx_add_timer(ev, SNAPSHOT_INTERVAL);

    /*delete expired file*/
    if (header->timestamp % CLEAN_INTERVAL == 0) {
        snap_root = ss_ctx->ss_conf->snap_root;
        ngx_copy(snap_dir, snap_root.data, snap_root.len);
        ngx_proc_snapshot_clean(snap_dir, header->timestamp, ss_ctx->ss_conf->nslots);
    }

    /*check stream every 10s*/
    if (header->timestamp % CHECK_STREAM_INTERVAL == 0 )
        ngx_proc_snapshot_check(ss_ctx);

    /*reap child*/
    if (ss_ctx->terminate == 1) {
        ngx_proc_snapshot_reap_child(ss_ctx);
    }

    /*dump buffer*/
    udp_entry_t                 *entry;
    ngx_str_t                   channel;
    char                        test[64] = {0};
    //snapshot_circular_buffer_t  *cbuffer;
    
    if (ev->log->log_level >= NGX_LOG_INFO) {
        for (i = 0; i < header->udp_address.nelts; i++) {
            //cbuffer = header->cbuffer + i;
            entry = ((udp_entry_t*)(header->udp_address.elts) + i);
            ngx_log_error(NGX_LOG_INFO, ev->log, 0, "dump buffer:%s\n", entry->name.data);
            //ngx_snapshot_dump_buffer(cbuffer);
            channel.data = test;
            channel.len = entry->name.len - 1;
            ngx_copy(channel.data, entry->name.data, channel.len);
            
            if (ev->log->log_level >= NGX_LOG_DEBUG) {
                if (0 != ngx_snapshot_raw(header, channel, header->timestamp - 2))
                ngx_log_error(NGX_LOG_INFO, ev->log, 0, "dump raw image error:%s/%ld\n", entry->name.data, header->timestamp - 2);
                if (0 != ngx_snapshot_large(header, channel, header->timestamp - 2))
                ngx_log_error(NGX_LOG_INFO, ev->log, 0, "dump large image error:%s/%ld\n", entry->name.data, header->timestamp - 2);
                if (0 != ngx_snapshot_small(header, channel, header->timestamp - 2))
                ngx_log_error(NGX_LOG_INFO, ev->log, 0, "dump large image error:%s/%ld\n", entry->name.data, header->timestamp - 2);
            }
        }
    }
    
}


static ngx_int_t
ngx_proc_snapshot_child_routine(void* args)
{
    snapshot_circular_buffer_t    *cbuffer;
    cbuffer                 = (snapshot_circular_buffer_t*)args;
    ngx_setproctitle(cbuffer->entry->name.data);
    /*for debug purpose*/
    /*register signals*/
    if (ngx_proc_snapshot_init_signals(ngx_cycle->log) == NGX_ERROR)
        return NGX_ERROR; 

    return ngx_snapshot_decode_stream(cbuffer);
}


static void 
ngx_proc_snapshot_clean(const char *path, time_t curr_time, ngx_uint_t lifetime)
{
    char                filename[FILENAME_MAX_LEN] = {0};
    char                timestamp[FILENAME_MAX_LEN] = {0};
    char                subdir[PATH_MAX_LEN] = {0};
    struct dirent       *dirp, *sub_dirp;
    DIR                 *dp, *sub_dp;

    dp = opendir(path);                                         
    if (!dp) {
        fprintf(stderr, "snapshot_clean error. opendir error,errno:%d", errno);
        return;
    }

    while ((dirp = readdir(dp))) {                            
        if (strcmp(dirp->d_name, ".") == 0 || strcmp(dirp->d_name, "..") == 0)
            continue;                                

        if (dirp->d_type == DT_DIR) {
            /*clean raw size expired files*/
            sprintf(subdir, "%s/%s", path, dirp->d_name);

            sub_dp = opendir(subdir);                                         
            if (!sub_dp) {
                fprintf(stderr, "snapshot_clean error. opendir:%s error,errno:%d\n", subdir, errno);
                return;
            }
            while ((sub_dirp = readdir(sub_dp))) {
                if (strcmp(sub_dirp->d_name, ".") == 0 || strcmp(sub_dirp->d_name, "..") == 0 ||
                    strcmp(sub_dirp->d_name, "small") == 0 || strcmp(sub_dirp->d_name, "large") == 0)
                    continue;
                sprintf(filename, "%s/%s", subdir, sub_dirp->d_name);
                /*TODO unsafe fixed TIMESTAMP_LEN*/
                strncpy(timestamp, sub_dirp->d_name, TIMESTAMP_LEN);
                if (curr_time - (time_t)atol(timestamp) >= (time_t)lifetime)
                    unlink(filename);
            }
            closedir(sub_dp);

            /*clean path/small subdirectory expired files*/
            sprintf(subdir, "%s/%s/small", path, dirp->d_name);

            sub_dp = opendir(subdir);                                         
            if (!sub_dp) {
                fprintf(stderr, "snapshot_clean error. opendir error,errno:%d\n", errno);
                return;
            }
            while ((sub_dirp = readdir(sub_dp))) {
                if (strcmp(sub_dirp->d_name, ".") == 0 || strcmp(sub_dirp->d_name, "..") == 0)
                    continue;
                sprintf(filename, "%s/%s", subdir, sub_dirp->d_name);
                strncpy(timestamp, sub_dirp->d_name, TIMESTAMP_LEN);
                if (curr_time - (time_t)atol(timestamp) >= (time_t)lifetime)
                    unlink(filename);
            }
            closedir(sub_dp);

            /*clean path/large subdirectory expired file*/
            sprintf(subdir, "%s/%s/large", path, dirp->d_name); 
            sub_dp = opendir(subdir);                                         
            if (!sub_dp) {
                fprintf(stderr, "snapshot_clean error. opendir error,errno:%d\n", errno);
                return;
            }
            while ((sub_dirp = readdir(sub_dp))) {
                if (strcmp(sub_dirp->d_name, ".") == 0 || strcmp(sub_dirp->d_name, "..") == 0)
                    continue;
                sprintf(filename, "%s/%s", subdir, sub_dirp->d_name);
                strncpy(timestamp, sub_dirp->d_name, TIMESTAMP_LEN);
                if (curr_time - (time_t)atol(timestamp) >= (time_t)lifetime)
                    unlink(filename);
            }
            closedir(sub_dp);
        }
    }

    closedir(dp);

}

void    
ngx_proc_snapshot_signal_handler(int signo)
{
    ngx_pid_t                   pid;
    ngx_err_t                   err;
    ngx_uint_t                  i, nchild;
    int                         status;
    ngx_proc_snapshot_conf_t    *ss_conf;
    snapshot_shm_header_t       *header;


    ss_conf = ngx_proc_get_conf(ngx_cycle->conf_ctx, ngx_proc_snapshot_module);
    header = ss_conf->ss_ctx->shm_header;
    nchild = header->udp_address.nelts;

    ngx_log_error(NGX_LOG_EMERG, ngx_cycle->log, 0,
                            "%d snapshot received signal:%d", getpid(), signo);

    switch (signo) {

    case SIGCHLD:
        for ( ;; ) {
            pid = waitpid(-1, &status, WNOHANG);
            if (pid == 0)
                return;
            if (pid == -1) {
                err = ngx_errno;
                if (err == NGX_EINTR) 
                    continue;
                ngx_log_error(NGX_LOG_INFO, ngx_cycle->log, err,
                                    "waitpid() failed");
                break;
            }

            if (ss_conf->ss_ctx->terminate == 0) {
                /*unexpected termination, restart child*/
                for (i = 0; i < nchild; i++) {
                    if (*(header->children_pid + i) == pid)
                        break;
                }
                
                if (i == nchild) {
                    ngx_log_error(NGX_LOG_EMERG, ngx_cycle->log, 0,
                                    "snapshot restart child failed");
                } else {
                    ngx_log_error(NGX_LOG_EMERG, ngx_cycle->log, 0,
                                    "snapshot child:%d crashed,restart it", pid);
                    ngx_proc_snapshot_dofork(header, i);
                }

            } else {
                /*expected termination*/
                for (i = 0; i < nchild; i++) {
                    if (*(header->children_pid + i) == pid)
                        break;
                }
                if (i < nchild) {
                    *(header->children_pid + i) = 0;
                    ngx_log_error(NGX_LOG_INFO, ngx_cycle->log, 0, 
                                        "snapshot child:%d reaped", pid);
                }
            }

        }
        break;
    case SIGINT:
    case SIGHUP:
    case SIGQUIT:
    case SIGTERM:
    case SIGUSR1:
    case SIGUSR2:
    case SIGWINCH:
    default:
        if (getpid() < *(header->children_pid)) {
            ss_conf->ss_ctx->terminate = 1;
            ngx_proc_snapshot_stop_child(ss_conf->ss_ctx);
        }
    }
}

void    
ngx_proc_snapshot_stop_child(ngx_proc_snapshot_ctx_t *ss_ctx)
{
    snapshot_shm_header_t           *header;
    ngx_uint_t                      i, nchild;
    ngx_int_t                       rc;

    header = ss_ctx->shm_header;
    nchild = header->udp_address.nelts;

    for (i = 0; i < nchild; i++)
    {
        if (*(header->children_pid + i) > 1) {
            rc = kill(*(header->children_pid + i), SIGKILL);
            if (rc == -1)
                ngx_log_error(NGX_LOG_EMERG, ngx_cycle->log, 0,
                            "kill child process error:%d", errno);
        }
    }
     
}

void
ngx_proc_snapshot_reap_child(ngx_proc_snapshot_ctx_t *ss_ctx)
{
    snapshot_shm_header_t           *header;
    ngx_err_t                       err;
    ngx_uint_t                      i, nchild;
    ngx_int_t                       rc, prepared;
    ngx_pid_t                       pid;
    int                             status;
    

    header = ss_ctx->shm_header;
    nchild = header->udp_address.nelts;
    prepared = 1;
    /*reap child*/
    for (i = 0; i < nchild; i++) {

        pid = *(header->children_pid + i);
        if (pid == 0)
            continue;

        prepared = 0;
        rc = waitpid(pid, &status, WNOHANG);
        if (rc == 0)
            continue;
        if (rc == -1) {
            err = ngx_errno;
            if (err == NGX_EINTR) 
                continue;
            if (err == NGX_ECHILD) {
                *(header->children_pid + i) = 0;
                continue;
            }
        }

        if (rc == pid) {
            *(header->children_pid + i) = 0;
            ngx_log_error(NGX_LOG_INFO, ngx_cycle->log, 0, 
                                "snapshot child:%d reaped", pid);
        }
    }

    if (prepared == 1) 
        exit(0);
    
}

ngx_int_t    
ngx_proc_snapshot_init_signals(ngx_log_t* log)
{
    struct sigaction            sa;
    int                         signals[8] = {SIGINT, SIGTERM, SIGQUIT, SIGHUP, SIGWINCH, SIGUSR1, SIGUSR2, SIGCHLD};
    int                         i, length = 8;

    for (i = 0; i < length; i++) {
        ngx_memzero(&sa, sizeof(struct sigaction));
        sa.sa_handler = ngx_proc_snapshot_signal_handler;
        sigemptyset(&sa.sa_mask);
        if (sigaction(signals[i], &sa, NULL) == -1) {
            ngx_log_error(NGX_LOG_EMERG, log, ngx_errno,
                        "sigaction %d failed", signals[i]);
            return NGX_ERROR;
        }
    }
    
    return NGX_OK;
}

void    
ngx_proc_snapshot_check(ngx_proc_snapshot_ctx_t *ss_ctx)
{
    snapshot_circular_buffer_t      *cbuffer;
    snapshot_shm_header_t           *header;
    ngx_uint_t                      i, threshold;
    snapshot_slot_t                 *slot;
    
    header = ss_ctx->shm_header;
    threshold = ss_ctx->ss_conf->missed_warning;
    for (i = 0; i < header->udp_address.nelts; i++) {
        cbuffer = header->cbuffer + i;
        slot = ngx_snapshot_prev_slot(cbuffer, cbuffer->index);
        if (slot->ts + (ngx_int_t)threshold < header->timestamp)
            ngx_log_error(NGX_LOG_ERR, ngx_cycle->log, 0,
               "channel:%s has missed decoding stream for %d seconds.",
               cbuffer->entry->name.data, header->timestamp - slot->ts);
        
    }

}



