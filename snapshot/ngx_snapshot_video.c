#include    "ngx_snapshot_video.h"


ngx_int_t
ngx_snapshot_decode_stream(snapshot_circular_buffer_t* cbuffer)
{
    if (!cbuffer){
        fprintf(stderr, "ngx_snapshot_decode_stream error.cbuffer is NULL");
        return -1;
    }

    u_char *recv_address    = cbuffer->entry->address.data;
    ngx_uint_t *pflag       = cbuffer->extract_flag;

    /*
     * decode the H264 stream and encode into jpg use ffmpeg.
     *
    */
    AVFormatContext *pFormatCtx = NULL;
    int             videoStream;
    AVCodecContext  *pCodecCtx = NULL;
    AVCodec         *pCodec = NULL;
    AVFrame         *pFrame = NULL;
    AVFrame         *pFrameRGB = NULL;
    AVPacket        packet;
    int             frameFinished;
    uint8_t         *buffer = NULL;
    uint32_t        i;

    AVDictionary    *optionsDict = NULL;

    av_register_all();
    avformat_network_init();

    if( avformat_open_input(&pFormatCtx, (const char*)recv_address, NULL, NULL) != 0 )
        return -1;

    if( avformat_find_stream_info(pFormatCtx, NULL) < 0 )
        return -1;

    //av_dump_format(pFormatCtx, 0, argv[1], 0);

    videoStream = -1;
    for(i=0; i<pFormatCtx->nb_streams; i++)
          if( pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO ) {
              videoStream=i;
              break;
          }

    if( videoStream == -1)
        return -1;

    pCodecCtx = pFormatCtx->streams[videoStream]->codec;

    pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
    if( pCodec == NULL ) {
        fprintf(stderr, "Unsupported codec!\n");
        return -1; // Codec not found
    }
    // Open codec
    if( avcodec_open2(pCodecCtx, pCodec, &optionsDict) < 0 )
        return -1; // Could not open codec

    // Allocate video frame
    pFrame = av_frame_alloc();

    i=0;
    while(av_read_frame(pFormatCtx, &packet)>=0) {
      if(packet.stream_index==videoStream) {
        avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished,
                     &packet);

        if(frameFinished) {
            if(*pflag == 1)
            {
                ngx_snapshot_write_jpeg(cbuffer, pCodecCtx, pFrame);
                *pflag = 0;
            }

        }//end of frameFinished.

      }

      // Free the packet that was allocated by av_read_frame
      av_free_packet(&packet);
    }

    // Free the RGB image
    av_free(buffer);
    av_free(pFrameRGB);

    // Free the YUV frame
    av_free(pFrame);

    // Close the codec
    avcodec_close(pCodecCtx);

    // Close the video file
    avformat_close_input(&pFormatCtx);

    return 0;
}

ngx_int_t
ngx_snapshot_write_jpeg(snapshot_circular_buffer_t *cbuffer, AVCodecContext *pAVCodecCtx, AVFrame *pFrame){

    AVCodecContext          *pCodecCtx = NULL;
    AVCodec                 *pCodec = NULL;
    AVDictionary            *optionsDict = NULL;
    AVPacket                packet;
    int                     got_packet = 0;
    int                     buf_size_actual;
    int                     ImgFmt = AV_PIX_FMT_YUVJ420P; //for the newer ffmpeg version, this int to pixelformat



    av_init_packet(&packet);
    packet.data = NULL;
    packet.size = 0;
    pCodec = avcodec_find_encoder(AV_CODEC_ID_MJPEG);
    if (!pCodec) {
        avcodec_close(pCodecCtx);
        av_free(pCodecCtx);
        return 0;
    }

    pCodecCtx = avcodec_alloc_context3(pCodec);
    if (!pCodecCtx){
        return 0;
    }

    pCodecCtx->bit_rate      = pAVCodecCtx->bit_rate;
    pCodecCtx->width         = pAVCodecCtx->width;
    pCodecCtx->height        = pAVCodecCtx->height;
    pCodecCtx->pix_fmt       = ImgFmt;
    pCodecCtx->codec_id      = AV_CODEC_ID_MJPEG;
    pCodecCtx->codec_type    = AVMEDIA_TYPE_VIDEO;
    pCodecCtx->time_base.num = pAVCodecCtx->time_base.num;
    pCodecCtx->time_base.den = pAVCodecCtx->time_base.den;

    pCodecCtx->codec = pCodec;
    if (avcodec_open2(pCodecCtx, pCodec, &optionsDict) < 0 ) {
            avcodec_close(pCodecCtx);
            av_free(pCodecCtx);
            return 0;

    }

    pCodecCtx->mb_lmin = pCodecCtx->lmin = pCodecCtx->qmin * FF_QP2LAMBDA;
    pCodecCtx->mb_lmax = pCodecCtx->lmax = pCodecCtx->qmax * FF_QP2LAMBDA;
    pCodecCtx->flags = CODEC_FLAG_QSCALE;
    pCodecCtx->global_quality = pCodecCtx->qmin * FF_QP2LAMBDA;

    pFrame->pts     = 1;
    pFrame->quality = pCodecCtx->global_quality;
    avcodec_encode_video2(pCodecCtx, &packet, pFrame, &got_packet);
    buf_size_actual = packet.size;

    ngx_snapshot_insert_buffer(cbuffer, packet.data, packet.size);

    av_free_packet(&packet);
    avcodec_close(pCodecCtx);
    av_free(pCodecCtx);
    return  buf_size_actual;
}

ngx_int_t
ngx_snapshot_slot_crop_image(snapshot_slot_t* slot, const char* outfile, size_t width, size_t height, ssize_t x, ssize_t y)
{

    MagickBooleanType       status;
    MagickWand              *magick_wand;

    if (!slot || !slot->data || !outfile) {
        fprintf(stderr, "crop_image error. slot, slot->data or outfile is NULL");
    }

    MagickWandGenesis();
    magick_wand=NewMagickWand();
    status = MagickReadImageBlob(magick_wand, slot->data, slot->size);
    if (status == MagickFalse)
        ThrowWandException(magick_wand);

    MagickResetIterator(magick_wand);
    while (MagickNextImage(magick_wand) != MagickFalse)
        MagickCropImage(magick_wand, width, height, x, y);
    /*
      Write the image then destroy it.
    */
    status=MagickWriteImages(magick_wand, outfile,MagickTrue);
    if (status == MagickFalse)
        ThrowWandException(magick_wand);
    magick_wand=DestroyMagickWand(magick_wand);
    MagickWandTerminus();

    return 0;
}

ngx_int_t
ngx_snapshot_slot_resize_image(snapshot_slot_t* slot, const char* outfile, size_t width, size_t height)
{

    MagickBooleanType       status;
    MagickWand              *magick_wand;

    if (!slot || !slot->data || !outfile) {
        fprintf(stderr, "crop_image error. slot, slot->data or outfile is NULL");
    }

    MagickWandGenesis();
    magick_wand=NewMagickWand();
    status = MagickReadImageBlob(magick_wand, slot->data, slot->size);
    if (status == MagickFalse)
        ThrowWandException(magick_wand);

    MagickResetIterator(magick_wand);
    while (MagickNextImage(magick_wand) != MagickFalse)
        MagickResizeImage(magick_wand, width, height,LanczosFilter,1.0);
    /*
      Write the image then destroy it.
    */
    status=MagickWriteImages(magick_wand, outfile, MagickTrue);
    if (status == MagickFalse)
        ThrowWandException(magick_wand);
    magick_wand=DestroyMagickWand(magick_wand);
    MagickWandTerminus();

    return 0;
}

ngx_int_t
ngx_snapshot_orig2style(snapshot_slot_t *slot, const char* outfile, STYLE s)
{
    MagickBooleanType       status;
    MagickWand              *magick_wand;

    if (!slot || !slot->data || !outfile) {
        fprintf(stderr, "raw2large error. slot, slot->data or outfile is NULL");
    }

    MagickWandGenesis();
    magick_wand=NewMagickWand();
    status = MagickReadImageBlob(magick_wand, slot->data, slot->size);
    if (status == MagickFalse)
        ThrowWandException(magick_wand);

    MagickResetIterator(magick_wand);
    while (MagickNextImage(magick_wand) != MagickFalse){

        switch(s)
        {
            case SMALL:
                MagickResizeImage(magick_wand, RAW_WIDTH, RAW_HEIGHT, LanczosFilter, 1.0);
                MagickCropImage(magick_wand, RAW_WIDTH - 2 * CROP_OFFSET_X,
                            RAW_WIDTH - 2 * CROP_OFFSET_Y, CROP_OFFSET_X, CROP_OFFSET_Y);
                MagickResizeImage(magick_wand, SMALL_WIDTH, SMALL_HEIGHT, LanczosFilter, 1.0);
                break;
            case LARGE:
                MagickResizeImage(magick_wand, RAW_WIDTH, RAW_HEIGHT, LanczosFilter, 1.0);
                MagickCropImage(magick_wand, RAW_WIDTH - 2 * CROP_OFFSET_X,
                            RAW_WIDTH - 2 * CROP_OFFSET_Y, CROP_OFFSET_X, CROP_OFFSET_Y);
                MagickResizeImage(magick_wand, LARGE_WIDTH, LARGE_HEIGHT, LanczosFilter, 1.0);
                break;
            case RAW:
                MagickResizeImage(magick_wand, RAW_WIDTH, RAW_HEIGHT, LanczosFilter, 1.0);
                break;
            default:
                fprintf(stderr, "convert image error, invalid image size style\n");
        }
    }
    /*
      Write the image then destroy it.
    */
    status=MagickWriteImages(magick_wand, outfile, MagickTrue);
    if (status == MagickFalse)
        ThrowWandException(magick_wand);
    magick_wand=DestroyMagickWand(magick_wand);
    MagickWandTerminus();

    return 0;

}

ngx_int_t
ngx_snapshot_raw(snapshot_shm_header_t *header, ngx_str_t channel, time_t target_ts)
{
    snapshot_slot_t         *slot;
    u_char                  outfile[FILENAME_MAX_LEN] = {0};

    memcpy(outfile, header->snap_root.data, header->snap_root.len);
    sprintf(outfile + header->snap_root.len, "/%s/%d.jpg", channel.data, (int32_t)target_ts);

    if (!(slot = ngx_snapshot_retrieve_slot(header, channel, target_ts)))
        return -1;

    if (slot->size == 0) {
        fprintf(stderr, "encounter empty slot, slot->ts:%ld", target_ts);
        return -1;
    }

    return ngx_snapshot_orig2style(slot, outfile, RAW);

}

ngx_int_t
ngx_snapshot_large(snapshot_shm_header_t *header, ngx_str_t channel, time_t target_ts)
{
    snapshot_slot_t         *slot;
    u_char                  outfile[FILENAME_MAX_LEN] = {0};

    memcpy(outfile, header->snap_root.data, header->snap_root.len);
    sprintf(outfile + header->snap_root.len, "/%s/large/%d.jpg", channel.data, (int32_t)target_ts);

    if (!(slot = ngx_snapshot_retrieve_slot(header, channel, target_ts)))
        return -1;
    if (slot->size == 0) {
        fprintf(stderr, "encounter empty slot, slot->ts:%ld", target_ts);
        return -1;
    }

    return ngx_snapshot_orig2style(slot, outfile, LARGE);
}

ngx_int_t
ngx_snapshot_small(snapshot_shm_header_t *header, ngx_str_t channel, time_t target_ts)
{
    snapshot_slot_t         *slot;
    u_char                  outfile[FILENAME_MAX_LEN] = {0};

    memcpy(outfile, header->snap_root.data, header->snap_root.len);
    sprintf(outfile + header->snap_root.len, "/%s/small/%d.jpg", channel.data, (int32_t)target_ts);

    if (!(slot = ngx_snapshot_retrieve_slot(header, channel, target_ts)))
        return -1;
    if (slot->size == 0) {
        fprintf(stderr, "encounter empty slot, slot->ts:%ld", target_ts);
        return -1;
    }

    return ngx_snapshot_orig2style(slot, outfile, SMALL);
}
