#include    "ngx_snapshot_shm.h"

void*
ngx_snapshot_get_shm(ngx_str_t mmap_file)
{
    
    ngx_int_t                   fd, flength;
    void                        *shm_addr;

    fd = open((char*)mmap_file.data, O_RDWR | O_CREAT , 00777);
    if ( fd == -1) {
        fprintf(stderr, "get snapshot shm error. can't create mmap file");
        return NULL;
    }

    flength = lseek(fd, 0, SEEK_END);
    write(fd, "", 1);
    shm_addr = mmap(NULL, flength, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    close(fd);

    if (shm_addr == (void*)-1)
    {
        fprintf(stderr, "get snapshot shm error. mmap error,errno:%d", errno);
        return NULL;
    }

    return shm_addr;

}

ngx_int_t 
ngx_snapshot_init_shm(snapshot_shm_header_t **header, ngx_str_t mmap_file, ss_array_t udp_address, ngx_str_t snap_root, ngx_uint_t nslots, ngx_uint_t *shm_size)
{
    /*alloc shared memory
    *   memory layout:
    *       snapshot_shm_header_t * 1
    *       snapshot_shm_header_t data
    *       snapshot_circular_buffer_t * n (number of udp_address)
    *       (snapshot_slot_t * nslots) * n 
    *       (u_char * SNAPSHOT_RAW_SIZE * nslots) * n
    */
    snapshot_circular_buffer_t  *cbuffer;
    snapshot_shm_header_t       *shm_header;
    snapshot_slot_t             *slot;
    ngx_int_t                   buffer_offset, slot_offset, total_size;
    ngx_uint_t                  naddress;
    ngx_int_t                   fd, flength;
    ngx_uint_t                  i, j;
    udp_entry_t                 *entry, *src_entry;
    void                        *shm_addr;

    naddress = udp_address.nelts;

    total_size = sizeof(snapshot_shm_header_t);
    /*add udp_address content */
    for (i = 0; i < naddress; i++) {
        entry = (udp_entry_t*)(udp_address.elts) + i;
        total_size += sizeof(udp_entry_t) + entry->name.len + entry->address.len;
    }
      

    /*add mmap_file content*/
    total_size += mmap_file.len;
    /*add snap_root content*/
    total_size += snap_root.len;
    /*add extract_flag array */
    total_size += sizeof(ngx_uint_t) * naddress;
    /*add children_pid array */
    total_size += sizeof(pid_t) * naddress;

    /*add snapshot_circular_buffer */
    total_size += (sizeof(snapshot_circular_buffer_t) + nslots * sizeof(snapshot_slot_t) + nslots * SNAPSHOT_RAW_SIZE) * naddress;

    fd = open((char*)mmap_file.data, O_RDWR | O_CREAT , 00777);
    if ( fd == -1) {
        fprintf(stderr, "snapshot create mmap file error");
        return NGX_ERROR;
    }

    flength = lseek(fd, total_size, SEEK_SET);
    write(fd, "", 1);
    shm_addr = mmap(NULL, flength, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
    close(fd);

    if (shm_addr == (void*)-1)
    {
        fprintf(stderr, "snapshot mmap error");
        return NGX_ERROR;
    }
    *shm_size = flength;
    *header = (snapshot_shm_header_t*)shm_addr;
    /*init snapshot_shm_header_t
    *   content layout:
    *       snapshot_shm_header_t 
    *       udp_address->elts * n
    *       udp_address->elts->data * n
    *       mmap_file->data
    *       snap_root->data
    *       extract_flag array
    *       children_pid array
    */
    shm_header = (snapshot_shm_header_t*)shm_addr;
    shm_header->shm_size = flength;
    shm_header->nslots = nslots;
    shm_header->shm_pos = (u_char*)shm_header + sizeof(snapshot_shm_header_t); 

    /*init shm_header->udp_address*/
    shm_header->udp_address.nelts = 0;
    shm_header->udp_address.size = sizeof(udp_entry_t);
    shm_header->udp_address.elts = shm_header->shm_pos;

    shm_header->shm_pos += sizeof(udp_entry_t) * naddress;

    for (i = 0; i < naddress; i++) {
        src_entry = (udp_entry_t*)udp_address.elts + i;
        entry = (udp_entry_t*)((u_char *)(shm_header->udp_address).elts + (shm_header->udp_address).size * (shm_header->udp_address).nelts);
        (shm_header->udp_address).nelts++;
        entry->address.len = src_entry->address.len;
        entry->address.data = shm_header->shm_pos;
        memcpy(entry->address.data, src_entry->address.data, src_entry->address.len);
        shm_header->shm_pos += src_entry->address.len;

        entry->name.len = src_entry->name.len;
        entry->name.data = shm_header->shm_pos;
        memcpy(entry->name.data, src_entry->name.data, src_entry->name.len);
        shm_header->shm_pos += src_entry->name.len;
                
    }

    /*init mmap_file*/
    shm_header->mmap_file.data = shm_header->shm_pos;
    shm_header->mmap_file.len = mmap_file.len;
    memcpy(shm_header->mmap_file.data, mmap_file.data, mmap_file.len);
    shm_header->shm_pos += mmap_file.len;

    /*init snap_root*/
    shm_header->snap_root.data = shm_header->shm_pos;
    shm_header->snap_root.len = snap_root.len;
    memcpy(shm_header->snap_root.data, snap_root.data, snap_root.len);
    shm_header->shm_pos += snap_root.len;

    /*init extract_flag array*/
    shm_header->extract_flag = (ngx_uint_t*)shm_header->shm_pos;
    shm_header->shm_pos += sizeof(ngx_uint_t) * naddress;

    /*init children_pid array*/
    shm_header->children_pid = (pid_t*)shm_header->shm_pos;
    shm_header->shm_pos += sizeof(pid_t) * naddress;

    /*init cbuffer*/
    shm_header->cbuffer = (snapshot_circular_buffer_t*)shm_header->shm_pos;
    buffer_offset = naddress * sizeof(snapshot_circular_buffer_t);
    slot_offset = nslots * sizeof(snapshot_slot_t);

    for (i = 0; i < naddress; i++) {
        cbuffer                 = shm_header->cbuffer + i;
        cbuffer->entry          = (udp_entry_t*)(shm_header->udp_address.elts) + i;
        cbuffer->ts_curr        = &(shm_header->timestamp);
        cbuffer->extract_flag   = shm_header->extract_flag + i;
        cbuffer->pid            = shm_header->children_pid + i;
        cbuffer->nslots         = nslots;
        cbuffer->index          = 0;
        cbuffer->slots          = (void*)(shm_header->shm_pos + buffer_offset + slot_offset * i);
        cbuffer->sec_gap        = DEFAULT_SEC_GAP;
        for (j = 0; j < nslots; j++) {
            /*init snapshot_slot_t*/
            slot        = (snapshot_slot_t*)((snapshot_slot_t*)(cbuffer->slots) + j);
            slot->data  = (void*)(shm_header->shm_pos + buffer_offset + 
                    slot_offset * naddress + nslots * SNAPSHOT_RAW_SIZE * i + SNAPSHOT_RAW_SIZE * j);
            slot->size  = 0;
            slot->ts    = 0;

        }

    }

    /*everything is ok. 
     *munmap the share memory 
     *since this function is executed in the master process.
    */
    if(0 != munmap(shm_addr, flength)) {
        fprintf(stderr, "munmap error!errno:%d", errno);
        return NGX_ERROR;
    }

    return NGX_OK;

}

ngx_int_t 
ngx_snapshot_insert_buffer(snapshot_circular_buffer_t *buffer, void* data, uint32_t num)
{
    /*insert new data*/
    snapshot_slot_t     *slot;
    slot = (snapshot_slot_t*)((snapshot_slot_t*)(buffer->slots) + buffer->index);
    slot->ts = *(buffer->ts_curr);
    slot->size = num;
    memcpy(slot->data, data, num);

    /*update ts_curr and index.
    */
    if (buffer->index == buffer->nslots - 1)
        buffer->index = 0;
    else
        buffer->index++;

    return 0;
    
}

void
ngx_snapshot_dump_buffer(snapshot_circular_buffer_t *cbuffer)
{
    snapshot_slot_t             *slot;
    ngx_uint_t                  j, nslots;
    //FILE                        *fp;
    //char                        file_name[256] = {0};

    nslots = cbuffer->nslots;
    for (j = 0; j < nslots; j++) {
        slot = (snapshot_slot_t*) ((snapshot_slot_t*)(cbuffer->slots) + j);
        if (slot->size != 0) {
            fprintf(stderr, "%ld\n", (long)(slot->ts));

            /*if (log->log_level == NGX_LOG_INFO) {
                sprintf(file_name, "/tmp/snapshot/%s/%ld.jpg", cbuffer->entry->name.data, (long int)slot->ts);
                fp = fopen(file_name, "w+");
                if (!fp) {
                    ngx_log_error(NGX_LOG_EMERG, log, 0, "open file for dump error!");
                }

                fwrite(slot->data, 1, slot->size, fp);
                fclose(fp);
            }*/
        }
    }
        
}

void*   
ngx_snapshot_get_buffer_by_name(snapshot_shm_header_t *header, ngx_str_t channel)
{
    snapshot_circular_buffer_t      *cbuffer;
    ngx_uint_t                      i, naddress;
    
    naddress = header->udp_address.nelts;
    for (i = 0; i < naddress; i++) {
        cbuffer = header->cbuffer + i;
        /*cbuffr->entry->name is a ngx_str_t with '\0' at the end*/
        if (cbuffer->entry->name.len - 1 == channel.len &&
            strncmp(cbuffer->entry->name.data, channel.data, channel.len) == 0)
                return cbuffer;
    }

    return NULL;
}

void*   
ngx_snapshot_retrieve_slot(snapshot_shm_header_t *header, ngx_str_t channel, time_t target_ts)
{
    snapshot_circular_buffer_t      *cbuffer;
    snapshot_slot_t                 *slot;
    ngx_int_t                       nslots, pesudo_index_curr, offset;
    ngx_int_t                       target_idx;
    time_t                          pesudo_ts_curr;

    nslots = header->nslots;
    cbuffer = ngx_snapshot_get_buffer_by_name(header, channel);
    if (cbuffer) {  
        /*current index has a high probablity to be changed asynchronously,
        * so we use the previous one as an alternative.
        */
        pesudo_index_curr  = cbuffer->index;
        pesudo_index_curr -= 1;

        slot = ngx_snapshot_prev_slot(cbuffer, pesudo_index_curr + 1);
        pesudo_ts_curr = slot->ts;
        
        offset = pesudo_ts_curr - target_ts;

        /*exclude invalid offset*/
        /*<= to < to allow accessing the pesudo current slot when sec_gap = 0*/
        if (offset < (ngx_int_t)cbuffer->sec_gap || 
                offset >= (ngx_int_t)(cbuffer->nslots - cbuffer->sec_gap)) {
            return NULL;
        }

        target_idx = pesudo_index_curr - offset;
        if (target_idx < 0) {
            target_idx += nslots;
        }

        return (snapshot_slot_t*)cbuffer->slots + target_idx;
        
    }

    return NULL;

}

ngx_int_t 
ngx_snapshot_retrieve_name(snapshot_shm_header_t *header, ngx_str_t channel, time_t target_ts, uint32_t range, time_t* buffer)
{
    snapshot_circular_buffer_t      *cbuffer;
    snapshot_slot_t                 *slot;
    ngx_int_t                       nslots, pesudo_index_curr, offset, end;
    ngx_int_t                       i, pos, target_idx, count;
    time_t                          pesudo_ts_curr;

    /*the caller provide the buffer*/
    if (!buffer || range <= 0)
        return -1;
    pos = count = 0;
    nslots = header->nslots;
    cbuffer = ngx_snapshot_get_buffer_by_name(header, channel);
    if (cbuffer) {  
        /*current index has a high probablity to be changed asynchronously,
        * so we use the previous one as an alternative.
        */
        pesudo_index_curr  = cbuffer->index - 1;
        

        slot = ngx_snapshot_prev_slot(cbuffer, pesudo_index_curr + 1);
        pesudo_ts_curr = slot->ts;

        if (pesudo_ts_curr < target_ts)
            target_ts = pesudo_ts_curr;
        offset = pesudo_ts_curr - target_ts;

        /*<= to < to allow accessing the pesudo current slot when sec_gap = 0*/
        if (offset < (ngx_int_t)cbuffer->sec_gap)
            offset = (ngx_int_t)cbuffer->sec_gap;
        else if (offset >= (ngx_int_t)(cbuffer->nslots - cbuffer->sec_gap - 1)) 
            return -1;
        target_idx = pesudo_index_curr - offset;
        if (target_idx < 0)
            target_idx += nslots;

        end = offset + range;
        if (end >= (ngx_int_t)(cbuffer->nslots - cbuffer->sec_gap - 1))
            end = (ngx_int_t)(cbuffer->nslots - cbuffer->sec_gap);
        
        for (i = 0; i < end - offset; i++) {
            slot = (snapshot_slot_t*)cbuffer->slots + target_idx;
            if (slot && slot->ts != 0) {
                *(buffer + pos) = slot->ts;
                pos++;
                count++;
            }
            target_idx--;
            if (target_idx < 0)
                target_idx += nslots;
        }

        return count;

    }

    return -1;

}

snapshot_slot_t*
ngx_snapshot_next_slot(snapshot_circular_buffer_t *cbuffer, ngx_uint_t uindex)
{
    ngx_int_t       index;
    index = uindex;
    /*sanity check*/
    if (index > (ngx_int_t)cbuffer->nslots || index < 0)
        return NULL;

    index += 1;
    if (index == (ngx_int_t)cbuffer->nslots) {
        index -= cbuffer->nslots;
    }

    return (snapshot_slot_t*)cbuffer->slots + index;
    
}

snapshot_slot_t*
ngx_snapshot_prev_slot(snapshot_circular_buffer_t *cbuffer, ngx_uint_t uindex)
{
    ngx_int_t       index;
    index = uindex;
    /*sanity check*/
    if (index > (ngx_int_t)cbuffer->nslots || index < 0)
        return NULL;

    index -= 1;
    if (index < 0) {
        index += cbuffer->nslots;
    }

    return (snapshot_slot_t*)cbuffer->slots + index;
    
}






