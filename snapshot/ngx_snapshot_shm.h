#ifndef     H_SNAPSHOT_SHM_H__
#define     H_SNAPSHOT_SHM_H__ 

#include <time.h>
#include <stdint.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <ngx_core.h>
#include <ngx_config.h>


#define     SNAPSHOT_RAW_SIZE   60*1024
#define     SNAPSHOT_INTERVAL   1000
#define     TIMESTAMP_LEN       10
#define     CLEAN_INTERVAL      30
#define     CHECK_STREAM_INTERVAL   10
/*notice: the smaller the DEFAULT_SEC_GAP 
the more likely the data at the end of the circular buffer 
to be changed.*/
#define     DEFAULT_SEC_GAP     0
#define     DEFAULT_ADDRESS_NUM 60
#define     FILENAME_MAX_LEN    256
#define     PATH_MAX_LEN        256

#define     NGX_OK          0
#define     NGX_ERROR      -1

typedef struct ss_array_s     ss_array_t;
struct ss_array_s {
    void        *elts;
    ngx_uint_t   nelts;
    size_t       size;
};

typedef struct {
    u_char          *data;
    time_t          ts;
    ngx_uint_t      size;

} snapshot_slot_t;

typedef struct {
    ngx_str_t       address;
    ngx_str_t       name;

} udp_entry_t;

typedef struct {
    void            *slots;
    ngx_uint_t      nslots;
    ngx_uint_t      index;
    time_t          *ts_curr;
    ngx_uint_t      sec_gap;
    udp_entry_t     *entry;
    ngx_uint_t      *extract_flag;
    pid_t           *pid;

} snapshot_circular_buffer_t;

typedef struct {
    ss_array_t                  udp_address;
    ngx_str_t                   mmap_file;
    ngx_str_t                   snap_root;
    ngx_uint_t                  *extract_flag;/*extract_flag array*/
    pid_t                       *children_pid;
    ngx_uint_t                  nslots;
    time_t                      timestamp;
    snapshot_circular_buffer_t  *cbuffer;
    u_char                      *shm_pos;
    ngx_uint_t                  shm_size;

} snapshot_shm_header_t;

snapshot_slot_t* ngx_snapshot_next_slot(snapshot_circular_buffer_t *cbuffer, ngx_uint_t uindex);
snapshot_slot_t* ngx_snapshot_prev_slot(snapshot_circular_buffer_t *cbuffer, ngx_uint_t uindex);

void*   ngx_snapshot_get_shm(ngx_str_t mmap_file);
ngx_int_t ngx_snapshot_init_shm(snapshot_shm_header_t **header, ngx_str_t mmap_file, ss_array_t udp_address, ngx_str_t snap_root, ngx_uint_t nslots, ngx_uint_t *shm_size);
ngx_int_t ngx_snapshot_insert_buffer(snapshot_circular_buffer_t *buffer, void* data, uint32_t num);
void    ngx_snapshot_dump_buffer(snapshot_circular_buffer_t *cbuffer);
void*   ngx_snapshot_get_buffer_by_name(snapshot_shm_header_t *header, ngx_str_t channel);
void*   ngx_snapshot_retrieve_slot(snapshot_shm_header_t *header, ngx_str_t channel, time_t target_ts);
ngx_int_t ngx_snapshot_retrieve_name(snapshot_shm_header_t *header, ngx_str_t channel, time_t target_ts, uint32_t range, time_t* buffer);


#endif  /*H_SNAPSHOT_SHM_H__*/
